//
// Created by Arya Hadi on 5/24/18.
//

#ifndef RADIO_APP_CONTEXT_H
#define RADIO_APP_CONTEXT_H

#include <QQmlApplicationEngine>

#include <memory>

class AppContext {
 public:
  static QQmlApplicationEngine &get_qml_app_engine() {
    if (qml_app_engine_ == nullptr) {
      qml_app_engine_ = std::make_shared<QQmlApplicationEngine>();
    }
    return *qml_app_engine_;
  }

 private:
  static std::shared_ptr<QQmlApplicationEngine> qml_app_engine_;
};

#endif //RADIO_APP_CONTEXT_H
