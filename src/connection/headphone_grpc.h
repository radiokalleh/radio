//
// Created by Arya Hadi on 5/22/18.
//

#ifndef RADIO_HEADPHONE_GRPC_H
#define RADIO_HEADPHONE_GRPC_H

#include <memory>

#include <grpcpp/grpcpp.h>

#include "protos/proto-cpp/headphone.grpc.pb.h"
#include "models/audio.h"

class HeadphoneGrpc {
 public:
  static HeadphoneGrpc &GetInstance() {
    std::call_once(HeadphoneGrpc::once_flag_, []() {
      instance_.reset(new HeadphoneGrpc);
    });
    return *instance_;
  }

  void set_channel(std::shared_ptr<grpc::Channel> channel) {
    stub_ = protocols::HeadphoneService::NewStub(channel);
  }

  std::unique_ptr<std::vector<Audio>> GetMainPageList();

 private:
  HeadphoneGrpc() {}
  HeadphoneGrpc(const HeadphoneGrpc&) = delete;
  HeadphoneGrpc& operator=(const HeadphoneGrpc&) = delete;

  static std::unique_ptr<HeadphoneGrpc> instance_;
  static std::once_flag once_flag_;

  std::unique_ptr<protocols::HeadphoneService::Stub> stub_;
};

#endif //RADIO_HEADPHONE_GRPC_H
