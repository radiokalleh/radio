//
// Created by Arya Hadi on 5/22/18.
//

#include "headphone_grpc.h"

std::unique_ptr<HeadphoneGrpc> HeadphoneGrpc::instance_;
std::once_flag HeadphoneGrpc::once_flag_;

std::unique_ptr<std::vector<Audio>> HeadphoneGrpc::GetMainPageList() {
  grpc::ClientContext context;
  protocols::GetMainPageListRequest request;
  protocols::GetMainPageListResponse response;

  auto status = stub_->GetMainPageList(&context, request, &response);

  if (status.ok()) {
    std::vector<Audio> audios;
    for (auto i = 0; i < response.audiosdetails_size(); ++i) {
      auto details = response.audiosdetails(i);

      audios.emplace_back(details.id(),
                          details.title(),
                          details.artistname(),
                          "http://aryaha.com:8585/audiocovers/" + details.coverurl());
//                          "https://www.wakesmiles.org/wp-content/uploads/2017/10/placeholder-2.png");
    }
    return std::make_unique<std::vector<Audio>>(audios);
  } else {
    return nullptr;
  }
}
