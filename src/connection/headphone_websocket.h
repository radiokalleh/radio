//
// Created by Arya Hadi on 5/21/18.
//

#ifndef RADIO_HEADPHONEWEBSOCKET_H
#define RADIO_HEADPHONEWEBSOCKET_H

#include <QObject>
#include <QtWebSockets/QWebSocket>
#include <QThread>

#include "protos/proto-cpp/headphone.pb.h"

class HeadphoneWebsocket : public QThread {
 Q_OBJECT

 public:
  explicit HeadphoneWebsocket(const QUrl &url, const std::string &media_id);
  ~HeadphoneWebsocket();

  void run() override;

  int AvailableBytesCount() { return audio_buffer_.size(); }
  int GetBytes(char *data, int max_len);

 private Q_SLOTS:
  void OnConnected();
  void OnDisconnected();
  void OnBinaryMessageReceived(const QByteArray &message);

 private:
  std::string media_id_;
  QUrl url_;
  QByteArray audio_buffer_;
  std::shared_ptr<QWebSocket> socket_;
};

#endif //RADIO_HEADPHONEWEBSOCKET_H
