//
// Created by Arya Hadi on 5/21/18.
//

#include "headphone_websocket.h"

HeadphoneWebsocket::HeadphoneWebsocket(const QUrl &url, const std::string &media_id) : url_(url), media_id_(media_id) {
}

HeadphoneWebsocket::~HeadphoneWebsocket() {
  HeadphoneWebsocket::requestInterruption();
  HeadphoneWebsocket::quit();
  HeadphoneWebsocket::wait();
}

void HeadphoneWebsocket::run() {
  socket_ = std::make_shared<QWebSocket>();
  connect(socket_.get(), &QWebSocket::connected, this, &HeadphoneWebsocket::OnConnected, Qt::DirectConnection);
  connect(socket_.get(), &QWebSocket::disconnected, this, &HeadphoneWebsocket::OnDisconnected, Qt::DirectConnection);
  connect(socket_.get(),
          &QWebSocket::binaryMessageReceived,
          this,
          &HeadphoneWebsocket::OnBinaryMessageReceived,
          Qt::DirectConnection);
  socket_->open(url_);

  exec();
}

void HeadphoneWebsocket::OnConnected() {
  protocols::PlayRequest play_request;
  play_request.set_mediaid(media_id_);
  QByteArray serialized = QByteArray::fromStdString(play_request.SerializeAsString());
  socket_->sendBinaryMessage(serialized);
}

void HeadphoneWebsocket::OnDisconnected() {
}

void HeadphoneWebsocket::OnBinaryMessageReceived(const QByteArray &message) {
  protocols::AudioBulk bulk;
  bulk.ParseFromArray(message, message.size());

  QByteArray byteArrayData(bulk.data().c_str(), bulk.data().length());
  audio_buffer_.append(byteArrayData);
}

int HeadphoneWebsocket::GetBytes(char *data, int max_len) {
  // FIXME: IT'S NOT THREAD-SAFE YET
  auto read_len = (max_len < audio_buffer_.size()) ? max_len : audio_buffer_.size();
  if (read_len == 0) {
    return 0;
  }

  memcpy(data, audio_buffer_.constData(), read_len);
  audio_buffer_ = audio_buffer_.right(audio_buffer_.size() - read_len);
  return read_len;
}

