#include <QGuiApplication>

#include <grpcpp/grpcpp.h>

#include "connection/headphone_grpc.h"
#include "ui/views/main_view.h"

void InitGrpcConnection() {
  auto channel = grpc::CreateChannel("aryaha.com:11000", grpc::InsecureChannelCredentials());
  HeadphoneGrpc::GetInstance().set_channel(channel);
}

int main(int argc, char *argv[]) {
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QGuiApplication app(argc, argv);

  // Connect to Headphone's GRPC Server
  InitGrpcConnection();

  // Inflate main view
  MainView main_view;
  main_view.Show();
  
  return app.exec();
}
