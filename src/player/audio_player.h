//
// Created by Arya Hadi on 5/21/18.
//

#ifndef RADIO_AUDIOPLAYER_H
#define RADIO_AUDIOPLAYER_H

#include <QObject>
#include <QIODevice>
#include <QAudioOutput>

#include <memory>
#include <iostream>

class AudioPlayer : public QObject
{
 Q_OBJECT
 public:
  explicit AudioPlayer(std::shared_ptr<QIODevice> audio_provider, QObject *parent = nullptr);

  void Resume();
  void Stop();

 private:
  std::shared_ptr<QIODevice> audio_provider_device_;
  std::shared_ptr<QAudioOutput> audio_out_;
};

#endif //RADIO_AUDIOPLAYER_H
