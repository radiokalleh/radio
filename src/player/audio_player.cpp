//
// Created by Arya Hadi on 5/21/18.
//

#include "audio_player.h"
#include <qdebug.h>

AudioPlayer::AudioPlayer(std::shared_ptr<QIODevice> audio_provider, QObject *parent) {
  Q_UNUSED(parent);

  QAudioFormat default_audio_format;
  default_audio_format.setSampleRate(44100);
  default_audio_format.setChannelCount(2);
  default_audio_format.setSampleSize(16);
  default_audio_format.setCodec("audio/pcm");
  default_audio_format.setByteOrder(QAudioFormat::LittleEndian);
  default_audio_format.setSampleType(QAudioFormat::SignedInt);

  QAudioDeviceInfo audio_device = QAudioDeviceInfo::defaultOutputDevice();
  if (!audio_device.isFormatSupported(default_audio_format)) {
    std::cerr << "format is not supported" << std::endl;
    default_audio_format = audio_device.nearestFormat(default_audio_format);
  }

  audio_out_ = std::make_shared<QAudioOutput>(audio_device, default_audio_format);
  audio_out_->start(audio_provider.get());
}

void AudioPlayer::Resume() {
  audio_out_->resume();
}

void AudioPlayer::Stop() {
  audio_out_->stop();
}
