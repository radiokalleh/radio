//
// Created by Arya Hadi on 5/25/18.
//

#ifndef RADIO_HEADPHONE_STREAMER_H
#define RADIO_HEADPHONE_STREAMER_H

#include <QIODevice>
#include <memory>

#include "connection/headphone_websocket.h"

class HeadphoneStreamer : public QIODevice {
 Q_OBJECT

 public:
  HeadphoneStreamer(std::string media_id);
  ~HeadphoneStreamer();

  qint64 writeData(const char *data, qint64 len) override {
    return 0;
  }
  qint64 readData(char *data, qint64 maxlen) override;
  qint64 bytesAvailable() const override;

 private:
  std::shared_ptr<HeadphoneWebsocket> websocket_;
};

#endif //RADIO_HEADPHONE_STREAMER_H
