//
// Created by Arya Hadi on 5/25/18.
//

#include "headphone_streamer.h"

HeadphoneStreamer::HeadphoneStreamer(std::string media_id) {
  open(QIODevice::ReadOnly);

  websocket_ = std::make_shared<HeadphoneWebsocket>(QUrl(QStringLiteral("ws://aryaha.com:10000/")), media_id);
  websocket_->start();
}

HeadphoneStreamer::~HeadphoneStreamer() {
}

qint64 HeadphoneStreamer::readData(char *data, qint64 maxlen) {
  return websocket_->GetBytes(data, maxlen);
}

qint64 HeadphoneStreamer::bytesAvailable() const {
  return websocket_->AvailableBytesCount() + QIODevice::bytesAvailable();
}
