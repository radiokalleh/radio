import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Window {
    id: main_view
    visible: true
    title: qsTr("Noise")
    width: 640
    height: 480

    signal itemClicked(string id)

    GridView {
        id: gridView
        anchors.fill: parent
        anchors.horizontalCenter: parent.horizontalCenter
        cellHeight: parent.width / 2 + 35
        cellWidth: parent.width / 2

        model: audio_list

        delegate: Item {

            Column {
                id: column
                x: 20
                y: 20
                spacing: 5
                width: parent.width - 20
                height: parent.height
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: 10
                anchors.topMargin: 10

                Image {
                    width: gridView.width / 2 - 20
                    fillMode: Image.PreserveAspectFit
                    source: cover

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            main_view.itemClicked(id_field.text)
                        }
                    }
                }
                Text {
                    text: audio_title
                    font.bold: true
                }
                Text {
                    text: artist
                }
                Text {
                    id: id_field
                    text: id
                    visible: false
                }
            }
        }
    }
}
