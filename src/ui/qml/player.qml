import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Layouts 1.3

Window {
    id: player_view
    visible: true
    title: qsTr("Noise - Player")
    width: 480
    height: 640

    ColumnLayout {
        id: columnLayout
        anchors.rightMargin: 20
        anchors.leftMargin: 20
        anchors.bottomMargin: 20
        anchors.topMargin: 20
        anchors.fill: parent

        Image {
            id: image
            height: 2 / 3 * player_view.height
            Layout.fillHeight: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            width: player_view.width - 40
            fillMode: Image.PreserveAspectFit
            source: cover
        }
        Text {
            text: audio_title
            font.pointSize: 21
            font.bold: true
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.fillWidth: true
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            width: player_view.width
        }
        Text {
            text: artist
            font.pointSize: 16
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            Layout.alignment: Qt.AlignHCenter | Qt.AlignVCenter
            Layout.fillWidth: true
            width: player_view.width
        }
    }

}
