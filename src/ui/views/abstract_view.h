//
// Created by Arya Hadi on 5/24/18.
//

#ifndef RADIO_ABSTRACT_VIEW_H
#define RADIO_ABSTRACT_VIEW_H

#include <QQmlContext>
#include <iostream>
#include <QStringLiteral>

#include "core/app_context.h"

class AbstractView : public QObject {
 public:
  AbstractView(QObject *parent = 0) {}

  virtual void Show() = 0;
};

#endif //RADIO_ABSTRACT_VIEW_H
