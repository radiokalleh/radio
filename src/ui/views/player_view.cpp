//
// Created by Arya Hadi on 5/25/18.
//

#include "player_view.h"

void PlayerView::Show() {
  QQmlContext *ctx = AppContext::get_qml_app_engine().rootContext();
  ctx->setContextProperty("audio_title", QString::fromStdString(selected_audio_->title()));
  ctx->setContextProperty("artist", QString::fromStdString(selected_audio_->artist()));
  ctx->setContextProperty("cover", QString::fromStdString(selected_audio_->cover_url()));

  AppContext::get_qml_app_engine().load(QUrl(QStringLiteral("qrc:src/ui/qml/player.qml")));

  auto *root_object = AppContext::get_qml_app_engine().rootObjects().last();
  QObject::connect(root_object, SIGNAL(closing(QQuickCloseEvent * )), this, SLOT(HandleOnClosing(QQuickCloseEvent * )));

  provider_ = std::make_shared<HeadphoneStreamer>(selected_audio_->id());
  audio_player_ = std::make_shared<AudioPlayer>(provider_);
}

void PlayerView::HandleOnClosing(QQuickCloseEvent *closeEvent) {
  Q_UNUSED(closeEvent)
  audio_player_.reset();

  // It should be run on another thread, because cleaning up takes time...
  QtConcurrent::run([=]() {
    provider_.reset();
  });
}
