//
// Created by Arya Hadi on 5/24/18.
//

#include <QQuickWindow>
#include "main_view.h"

void MainView::Show() {
  audios_list = HeadphoneGrpc::GetInstance().GetMainPageList();
  if (audios_list == nullptr) {
    std::cerr << "GetMainPageList returns nullptr" << std::endl;
    return;
  }

  // TODO: Fix this memory leak
  AudioListModel *main_audio_list = new AudioListModel();
  for (auto audio : *audios_list) {
    main_audio_list->AddAudio(audio);
  }

  QQmlContext *ctx = AppContext::get_qml_app_engine().rootContext();
  ctx->setContextProperty("audio_list", main_audio_list);

  AppContext::get_qml_app_engine().load(QUrl(QStringLiteral("qrc:src/ui/qml/main.qml")));

  auto *root_object = AppContext::get_qml_app_engine().rootObjects().last();

  QObject::connect(root_object, SIGNAL(itemClicked(QString)), this, SLOT(HandleClickedItem(QString)));
}

void MainView::HandleClickedItem(QString id) {
  for (auto audio : *audios_list) {
    if (audio.id() == id.toStdString()) {
      auto audio_ptr = std::make_shared<Audio>(audio.id(), audio.title(), audio.artist(), audio.cover_url());
      player = std::make_shared<PlayerView>(audio_ptr);
      break;
    }
  }

  if (player == nullptr) {
    std::cerr << "no matching audio found to start player" << std::endl;
    return;
  }

  player->Show();
}
