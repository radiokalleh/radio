//
// Created by Arya Hadi on 5/25/18.
//

#ifndef RADIO_PLAYER_VIEW_H
#define RADIO_PLAYER_VIEW_H

#include <QtQuick>
#include <QtConcurrent>

#include "ui/views/abstract_view.h"
#include "providers/headphone_streamer.h"
#include "connection/headphone_grpc.h"
#include "player/audio_player.h"
#include "models/audio.h"

class PlayerView : public AbstractView {
 Q_OBJECT

 public:
  PlayerView(std::shared_ptr<Audio> selected_audio) : AbstractView(), selected_audio_(selected_audio) {}

  void Show() override;

 public slots:
  void HandleOnClosing(QQuickCloseEvent *closeEvent);

 private:
  std::shared_ptr<Audio> selected_audio_;
  std::shared_ptr<HeadphoneStreamer> provider_;
  std::shared_ptr<AudioPlayer> audio_player_;
};

#endif //RADIO_PLAYER_VIEW_H
