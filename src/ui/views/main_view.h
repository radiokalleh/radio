//
// Created by Arya Hadi on 5/24/18.
//

#ifndef RADIO_MAIN_VIEW_H
#define RADIO_MAIN_VIEW_H

#include "ui/views/abstract_view.h"
#include "ui/views/player_view.h"
#include "connection/headphone_grpc.h"
#include "models/ui/audio_listmodel.h"

class MainView : public AbstractView {
 Q_OBJECT

 public:
  MainView() : AbstractView() {}

 public slots:
  void HandleClickedItem(QString id);

  void Show() override;

 private:
  std::unique_ptr<std::vector<Audio>> audios_list = nullptr;
  std::shared_ptr<PlayerView> player = nullptr;
};

#endif //RADIO_MAIN_VIEW_H
