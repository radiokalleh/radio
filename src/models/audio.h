//
// Created by Arya Hadi on 5/22/18.
//

#ifndef RADIO_AUDIO_H
#define RADIO_AUDIO_H

#include <string>

class Audio {
 public:
  Audio(const std::string &id,
        const std::string &title = "",
        const std::string &artist = "",
        const std::string &cover_url = "")
      : id_(id), title_(title), artist_(artist), cover_url_(cover_url) {}

  const std::string &id() const { return id_; }
  void set_id(const std::string &id) { id_ = id; }

  const std::string &title() const { return title_; }
  void set_title(const std::string &title) { title_ = title; }

  const std::string &artist() const { return artist_; }
  void set_artist(const std::string &artist) { artist_ = artist; }

  const std::string &cover_url() const { return cover_url_; }
  void set_cover_url(const std::string &cover_url) { cover_url_ = cover_url; }

 private:
  std::string id_, title_, artist_, cover_url_;
};

#endif //RADIO_AUDIO_H
