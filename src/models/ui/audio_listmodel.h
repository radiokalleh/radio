//
// Created by Arya Hadi on 5/25/18.
//

#ifndef RADIO_AUDIO_MODEL_H
#define RADIO_AUDIO_MODEL_H

#include <QAbstractListModel>
#include <QString>
#include <QByteArray>
#include <QObject>
#include <QList>

#include "models/audio.h"

enum AudioRoles {
  TitleRole = Qt::UserRole + 1,
  IdRole,
  ArtistRole,
  CoverRole
};

class AudioListModel : public QAbstractListModel {
 Q_OBJECT

 public:
  AudioListModel(QObject *parent = 0);

  void AddAudio(Audio audio);

  QHash<int, QByteArray> roleNames() const override;
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;

 private:
  QList<Audio> audios_;
};

#endif //RADIO_AUDIO_MODEL_H
