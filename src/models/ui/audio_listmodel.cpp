//
// Created by Arya Hadi on 5/25/18.
//

#include "audio_listmodel.h"

AudioListModel::AudioListModel(QObject *parent) : QAbstractListModel(parent) {
}

void AudioListModel::AddAudio(Audio audio) {
  beginInsertRows(QModelIndex(), rowCount(), rowCount());
  audios_ << audio;
  endInsertRows();
}

QHash<int, QByteArray> AudioListModel::roleNames() const {
  QHash<int, QByteArray> roles;
  roles[AudioRoles::TitleRole] = "audio_title";
  roles[AudioRoles::IdRole] = "id";
  roles[AudioRoles::ArtistRole] = "artist";
  roles[AudioRoles::CoverRole] = "cover";
  return roles;
}

int AudioListModel::rowCount(const QModelIndex &parent) const {
  return audios_.count();
}

QVariant AudioListModel::data(const QModelIndex &index, int role) const {
  if (index.row() < 0 || index.row() > audios_.count()) {
    return QVariant();
  }

  const Audio &audio = audios_[index.row()];
  if (role == AudioRoles::ArtistRole) {
    return QString::fromStdString(audio.artist());
  } else if (role == AudioRoles::IdRole) {
    return QString::fromStdString(audio.id());
  } else if (role == AudioRoles::TitleRole) {
    return QString::fromStdString(audio.title());
  } else if (role == AudioRoles::CoverRole) {
    return QString::fromStdString(audio.cover_url());
  }
  return QVariant();
}
