#-------------------------------------------------
#
# Project created by QtCreator 2018-04-21T10:06:39
#
#-------------------------------------------------

QT       += core gui network websockets multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = radio
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    audioprovider.cpp \
    protos/proto-cpp/streamer.pb.cc \
    protos/proto-cpp/version.generated.cc \
    audioplayer.cpp

HEADERS += \
        mainwindow.h \
    audioprovider.h \
    protos/proto-cpp/streamer.pb.h \
    audioplayer.h \
    util.h

FORMS += \
        mainwindow.ui

win32:CONFIG(release, debug|release): LIBS += -L/usr/local/lib/release -lprotobuf
else:win32:CONFIG(debug, debug|release): LIBS += -L/usr/local/lib/debug -lprotobuf
else:unix: LIBS += -L/usr/local/lib -lprotobuf

INCLUDEPATH += /usr/local/include
DEPENDPATH += /usr/local/include

win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += /usr/local/lib/release/libprotobuf.a
else:win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += /usr/local/lib/debug/libprotobuf.a
else:win32:!win32-g++:CONFIG(release, debug|release): PRE_TARGETDEPS += /usr/local/lib/release/protobuf.lib
else:win32:!win32-g++:CONFIG(debug, debug|release): PRE_TARGETDEPS += /usr/local/lib/debug/protobuf.lib
else:unix: PRE_TARGETDEPS += /usr/local/lib/libprotobuf.a
